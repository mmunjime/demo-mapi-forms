%dw 2.0
output application/problem+json
var apiName = vars.apiName
---
	{
	    "errorType"  : ("error/") ++ (error.errorType.namespace default "") ++ "/" ++ (error.errorType.identifier default ""),
	    "title":  if(error.exception.MESSAGE_TITLE != null) error.exception.MESSAGE_TITLE else error.description, 
	    "status" :  vars.httpStatus default "",
	    "detail": if(vars.errorDescription != null) 
	    	    	((vars.errorDescription default ""))
		    	  else 
		    		("Error on > " ++ (apiName default "") ++ "; Error: " ++ (error.detailedDescription default "")),  
	    "instance": ("error/") ++ (error.errorType.namespace default "") ++ "/" ++ (error.errorType.identifier default "") ,
	    "timestampUtc": now() as String,
	     ("causes": error.childErrors map {
	     	"errorType" : ($.errorType.namespace default "") ++ ":" ++ ($.errorType.identifier default ""),
	     	"title": $.description default "",
	    	"status" :  vars.httpStatus default "",
			"detail": ("Error on > " ++ (apiName default "") ++ "; Error: " ++ ($.detailedDescription default "")), 
			"instance": ("error/") ++ (error.errorType.namespace default "") ++ "/" ++ (error.errorType.identifier default ""),
			"timestampUtc": now() as String,
	    }) if(!isEmpty(error.childErrors))
	}